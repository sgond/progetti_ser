import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    static int port = 5000;

    public static void main(String[] args) throws IOException {
        Socket clientSocket = new Socket("127.0.0.1", port);
        System.out.println("Collegato");

        Scanner scanner = new Scanner(System.in);

        DataOutputStream toServer = new DataOutputStream((clientSocket.getOutputStream()));
        DataInputStream fromServer = new DataInputStream(clientSocket.getInputStream());
        while (true) {
            while (true) {
                System.out.println(fromServer.readUTF());
                toServer.writeInt(scanner.nextInt());

                String outServer = fromServer.readUTF();
                if (outServer.compareTo("0") == 0)              break;

                System.out.println(outServer);
                System.out.println(fromServer.readUTF());
            }

            System.out.println(fromServer);
            String decision = scanner.nextLine();

            toServer.writeUTF(decision);
            if(decision != "S")                     break;
        }
        clientSocket.close();
    }
}
