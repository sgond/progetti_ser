import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Random;

public class Server{
    static int port = 5000;

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Il server è acceso");
        while(true){
            try {

                Socket connection = serverSocket.accept();
                System.out.println("Utente collegato");

                Thread t = new Thread(new Session(connection));
                t.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static class Session implements Runnable{
        private Socket connection;

        private int[] numToGuess;
        private int[] numClient;

        private int numLength = 4, numReceived;
        private int numsSamePos, numsDiffPos;

        Session(Socket connection){
            this.connection = connection;
        }

        @Override
        public void run() {
            try {
                DataInputStream fromClient= new DataInputStream(connection.getInputStream());
                DataOutputStream toClient = new DataOutputStream(connection.getOutputStream());
                while (true) {
                    generateNum();
                    System.out.println(Arrays.toString(numToGuess));
                    toClient.writeUTF("Inserire numero: ");
                    numReceived = fromClient.readInt();
                    splitNum();
                    System.out.println(Arrays.toString(numClient));
                    System.out.println(numReceived);
                    while (true) {
                        checkSamePos();
                        checkDiffPos();

                        if (numsSamePos == numLength) {
                            toClient.writeUTF("0");
                            toClient.writeUTF("Hai indovinato il numero!");

                            break;
                        }

                        toClient.writeUTF("Numeri in posizione corretta:" + numsSamePos);
                        toClient.writeUTF("Numeri corretti ma in posizione errata:" + numsDiffPos);

                        toClient.writeUTF("Inserire nuovo numero: ");
                        numReceived = fromClient.read();
                        System.out.println(numReceived);
                    }

                    toClient.writeUTF("Fare nuova partita? (Inserire S per SI', qualsiasi cosa per NO)");
                    if(fromClient.readUTF() != "S")     break;
                }

                    connection.close();
                } catch (IOException e) {
                    System.err.println("Errore IO");
                }
        }


        private void splitNum(){
            numClient = splitNum(numReceived);
        }

        public int[] splitNum(int num){
            int[] out = new int[numLength];

            for (int i = 0; i < numLength; i++) {                                                                              //Partendo dall'ultima cifra, la si aggiunge all'ultimo posto dell'array
                out[numLength-(i+1)] = num % 10;
                num = num / 10;
            }

            return out;
        }

        private void generateNum(){
            numToGuess = generateNum(numLength);
        }

        public int[] generateNum(int numLength){
            int[] numToGuess = new int[numLength];
            Random random = new Random();

            for (int i = 0; i < numLength; i++) {
                numToGuess[i] = random.nextInt(10);

                for (int j = 0; j < i; j++)                                                                             //Controlla che non ci siano doppioni
                    if(numToGuess[j] == numToGuess[i]){
                        numToGuess[i] = random.nextInt(10);                                                      //Se trova un doppione, crea un nuovo numero e porta j a 0 per controllare che non lo sia nuovamente
                        j = 0;
                    }
            }

            return numToGuess;
        }

        private void checkSamePos(){
            numsSamePos = checkSamePos(numClient, numToGuess);
        }

        public int checkSamePos(int[] a, int[] b){
            int numsSamePos = 0;

            for (int i = 0; i < a.length; i++) {
                if(a[i] == b[i])
                    numsSamePos++;
            }

            return numsSamePos;
        }

        private void checkDiffPos(){
            numsDiffPos = checkDiffPos(numClient, numToGuess);
        }

        public int checkDiffPos(int[] a, int[] b){
            int numsDiffPos = 0;

            for (int i = 0; i < a.length; i++)
                for (int j = 0; j < b.length; j++)
                    if(a[i] == b[j] && i != j)
                        numsDiffPos++;

            return numsDiffPos;
        }

    }
}

