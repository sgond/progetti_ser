package bot;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class CSVLoader {

   private BufferedReader br;
   private BufferedWriter bw;

    private String selectedValue;
    private ArrayList<String> values = new ArrayList<>();

    private int selectedCol;

    public CSVLoader(String file) throws CSVLoaderException, IOException {
        if(file.contains(".csv")) {
            br = new BufferedReader(new InputStreamReader((getClass().getClassLoader().getResourceAsStream(file))));
            bw = new BufferedWriter(new FileWriter(getClass().getClassLoader().getResource(file).getPath()));
            setValues();
        }
        else throw new CSVLoaderException();
    }

    public CSVLoader(String file, String selectedValue) throws CSVLoaderException, IOException {
        if(file.contains(".csv")) {
            br = new BufferedReader(new InputStreamReader((getClass().getClassLoader().getResourceAsStream(file))));
            setValues();
            this.selectedValue = selectedValue;
            selectedCol = values.indexOf(selectedValue);
        }
        else throw new CSVLoaderException();
    }

    /**
     * @param file The name of the file (it must be in the resource folder)
     * @param selectedCol The column you want to collect (the columns starts from 0)
     * @throws CSVLoaderException
     * @throws IOException
     */
    public CSVLoader(String file, int selectedCol) throws CSVLoaderException, IOException {
        if(file.contains(".csv")){
            br = new BufferedReader(new InputStreamReader((getClass().getClassLoader().getResourceAsStream(file))));
            setValues();
            this.selectedCol = selectedCol;
            selectedValue = values.get(selectedCol);
        }else throw new CSVLoaderException();
    }

    private void setValues() throws IOException {
        String lineValues = br.readLine();
        if(lineValues != null)
            values.addAll(Arrays.asList(lineValues.split(";")));

        if(selectedValue != null)
            selectedValue = (String) values.get(0);
    }

    public ArrayList encodeCSV() throws IOException {
        ArrayList<String> out = new ArrayList();

        String line;
        String[] arrLine;

        while((line = br.readLine()) != null){
            arrLine = line.split(";");
            out.add(arrLine[selectedCol].toLowerCase());
        }
        return out;
    }

    public boolean writeOnCSV(String s){
        try {
            bw.write(s + "\n");
            bw.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeOnCSV(String s, String s2){
        try {
            bw.write(s + ";" + s2);
            bw.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
