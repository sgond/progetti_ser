package bot;

public class Libro {
    String tit;
    String aut;
    String idInv;
    String idAut;
    String argomento;
    String edi;
    String luo;
    String pub;

    public Libro(){}

    public Libro(String tit) {
        this.tit = tit;
    }

    public Libro(String aut, String idAut) {
        this.aut = aut;
        this.idAut = idAut;
    }

    public Libro(String tit, String aut, String idInv, String idAut) {
        this.tit = tit;
        this.aut = aut;
        this.idInv = idInv;
        this.idAut = idAut;
    }

    public Libro(String tit, String aut, String idInv, String idAut, String argomento, String edi, String luo, String pub) {
        this.tit = tit;
        this.aut = aut;
        this.idInv = idInv;
        this.idAut = idAut;
        this.argomento = argomento;
        this.edi = edi;
        this.luo = luo;
        this.pub = pub;
    }

    public void setTit(String tit) {
        this.tit = tit;
    }

    public void setAut(String aut) {
        this.aut = aut;
    }

    public void setIdInv(String idInv) {
        this.idInv = idInv;
    }

    public void setIdAut(String idAut) {
        this.idAut = idAut;
    }

    public void setArgomento(String argomento) {
        this.argomento = argomento;
    }

    public void setEdi(String edi) {
        this.edi = edi;
    }

    public void setLuo(String luo) {
        this.luo = luo;
    }

    public void setPub(String pub) {
        this.pub = pub;
    }
}
