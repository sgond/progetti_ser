package bot;

import java.io.IOException;
import java.util.ArrayList;

public class Searcher {
    private String mode;
    private Archives archives;
    {
        try {
            archives = new Archives();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CSVLoaderException e) {
            e.printStackTrace();
        }
    }

    public Searcher(String mode) {
        this.mode = mode;
    }

    public ArrayList<Libro> search(String search) {
        ArrayList<Libro> out = new ArrayList<>();

        switch (mode) {
            case "autors":
                ArrayList<String> idAuts = new ArrayList<>();
                ArrayList<String> autoriFound = new ArrayList<>();
                for (int i = 0; i < archives.getAutori().size(); i++)
                    if (archives.getAutori().get(i).contains(search.toLowerCase())) {
                        idAuts.add(archives.getAutori_idAut().get(i));
                        autoriFound.add(archives.getAutori().get(i));
                    }

                if (!idAuts.isEmpty()) {
                    for (int i = 0; i < archives.getAutoriLibro().size(); i++) {
                        String idAutToCheck = archives.getAutoriLibro().get(i);
                        for (int j = 0; j < idAuts.size(); j++)
                            if (idAuts.get(j).equals(idAutToCheck)) {
                                Libro toAdd = new Libro();
                                toAdd.setIdAut(idAuts.get(j));
                                toAdd.setAut(autoriFound.get(j));
                                toAdd.setIdInv(archives.getAutoriLibro_idInvs().get(i));
                                out.add(toAdd);
                            }
                    }

                    for (int i = 0; i < archives.getLibri_idInvs().size(); i++) {
                        String idInvToCheck = archives.getLibri_idInvs().get(i);
                        for (int j = 0; j < out.size(); j++) {
                            Libro libroToCheck = (Libro) out.get(j);
                            if (idInvToCheck.equals(libroToCheck.idInv)) {
                                libroToCheck.setTit(archives.getLibri().get(i));
                                libroToCheck.setPub(archives.getLibri_pub().get(i));
                                libroToCheck.setLuo(archives.getLibri_luo().get(i));
                                libroToCheck.setEdi(archives.getLibri_edi().get(i));
                            }
                        }
                    }
                    return out;
                }

                break;


            case "titles":

                for (int i = 0; i < archives.getLibri().size(); i++) {
                    String titToCheck = archives.getLibri().get(i);
                    if (titToCheck.contains(search.toLowerCase())) {
                        Libro toAdd = new Libro(titToCheck);
                        toAdd.setEdi(archives.getLibri_edi().get(i));
                        toAdd.setLuo(archives.getLibri_luo().get(i));
                        toAdd.setPub(archives.getLibri_pub().get(i));
                        toAdd.setIdInv(archives.getLibri_idInvs().get(i));
                        out.add(toAdd);
                    }
                }
                if (!out.isEmpty()) {
                    for (int i = 0; i < archives.getAutoriLibro_idInvs().size(); i++) {
                        String idInv = archives.getAutoriLibro_idInvs().get(i);
                        for (int j = 0; j < out.size(); j++) {
                            Libro libroToCheck = (Libro) out.get(j);
                            if (libroToCheck.idInv.equals(idInv))
                                libroToCheck.setIdAut(archives.getAutoriLibro().get(i));
                        }
                    }

                    for (int i = 0; i < archives.getAutori_idAut().size(); i++) {
                        String idAutToCheck = archives.getAutori_idAut().get(i);
                        for (int j = 0; j < out.size(); j++) {
                            Libro libroToCheck = (Libro) out.get(j);
                            if (libroToCheck.idAut.equals(idAutToCheck))
                                libroToCheck.setAut(archives.getAutori().get(i));
                        }
                    }
                    return out;
                }

                break;
        }
        return new ArrayList<>();
    }
}
