package bot;

import java.io.IOException;
import java.util.ArrayList;

public class Archives {
    private final String autori_csv = "csv/Autori.csv";
    private final String autoriLibro_csv = "csv/AutoriLibro.csv";
    private final String dewey_csv = "csv/Dewey.csv";
    private final String libri_csv = "csv/Libri.csv";
    private static final String prenotati_csv = "src/main/resources/csv/Prenotati.csv";
    private static final String prenotati_csvIS = "csv/Prenotati.csv";

    private final ArrayList<String> autori = new CSVLoader(autori_csv, 1).encodeCSV();                          //Aut
    private final ArrayList<String> autori_idAut = new CSVLoader(autori_csv, 0).encodeCSV();                    //IDAut
    private final ArrayList<String> autoriLibro = new CSVLoader(autoriLibro_csv, 2).encodeCSV();                //IDAut
    private final ArrayList<String> autoriLibro_idInvs = new CSVLoader(autoriLibro_csv, 1).encodeCSV();         //IDInv
    private final ArrayList<String> dewey = new CSVLoader(dewey_csv, 1).encodeCSV();
    private final ArrayList<String> libri = new CSVLoader(libri_csv, 2).encodeCSV();                            //Tit
    private final ArrayList<String> libri_idInvs = new CSVLoader(libri_csv, 0).encodeCSV();                     //IDInv
    private final ArrayList<String> libri_edi = new CSVLoader(libri_csv, 4).encodeCSV();
    private final ArrayList<String> libri_luo = new CSVLoader(libri_csv, 5).encodeCSV();
    private final ArrayList<String> libri_pub = new CSVLoader(libri_csv, 6).encodeCSV();
    private static ArrayList<String> prenotati;

    static {
        try {
            prenotati = new CSVLoader(prenotati_csvIS).encodeCSV();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CSVLoaderException e) {
            e.printStackTrace();
        }
    }

    public Archives() throws IOException, CSVLoaderException {}

    public String getAutori_csv() {
        return autori_csv;
    }

    public String getAutoriLibro_csv() {
        return autoriLibro_csv;
    }

    public String getDewey_csv() {
        return dewey_csv;
    }

    public String getLibri_csv() {
        return libri_csv;
    }

    public ArrayList<String> getAutori() {
        return autori;
    }

    public ArrayList<String> getAutori_idAut() {
        return autori_idAut;
    }

    public ArrayList<String> getAutoriLibro() {
        return autoriLibro;
    }

    public ArrayList<String> getAutoriLibro_idInvs() {
        return autoriLibro_idInvs;
    }

    public ArrayList<String> getDewey() {
        return dewey;
    }

    public ArrayList<String> getLibri() {
        return libri;
    }

    public ArrayList<String> getLibri_idInvs() {
        return libri_idInvs;
    }

    public ArrayList<String> getLibri_edi() {
        return libri_edi;
    }

    public ArrayList<String> getLibri_luo() {
        return libri_luo;
    }

    public ArrayList<String> getLibri_pub() {
        return libri_pub;
    }

    public ArrayList<String> getPrenotati() {
        return prenotati;
    }

    public static boolean WriteOnPrenotati(String s, String chatId){
        boolean out;
        try {
           out = new CSVLoader(prenotati_csvIS).writeOnCSV(s, chatId);
        } catch (CSVLoaderException e) {
            out = false;
        } catch (IOException e) {
            out = false;
        }
        try {
            prenotati = new CSVLoader(prenotati_csvIS).encodeCSV();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CSVLoaderException e) {
            e.printStackTrace();
        }

        return out;
    }
}
