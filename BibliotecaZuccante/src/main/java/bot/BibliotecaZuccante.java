package bot;

import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BibliotecaZuccante extends TelegramLongPollingBot {
    private ArrayList<Libro> libriFound = new ArrayList<>();

    private boolean isAutComm = false;
    private boolean autMode = false;
    private boolean isTitComm = false;
    private boolean titMode = false;
    private boolean prenotazione = false;

    SendMessage sendMessage = new SendMessage();

    private long chatId;

    public BibliotecaZuccante() {
        setButtons(sendMessage);
    }

    @Override
    public void onUpdateReceived(Update update) {
        if(update.hasMessage() && update.getMessage().hasText()){
            new Thread(() -> {
                String message = update.getMessage().getText();
                chatId = update.getMessage().getChatId();

                if(!isAutComm && !isTitComm && !prenotazione){
                    switch (message) {
                        case "Cerca Autore":
                            isAutComm = true;
                            autMode = true;
                            titMode = false;
                            isTitComm = false;

                            sendMsg(chatId, "Inserisci l'autore che desideri cercare");
                            break;

                        case "Cerca Titolo":
                            isTitComm = true;
                            titMode = true;
                            autMode = false;
                            isAutComm = false;

                            sendMsg(chatId, "Inserisci il libro che desideri cercare");
                            break;

                        case "Prenota":
                            if(libriFound.isEmpty())
                                sendMsg(chatId, "Non hai ancora fatto nessuna ricerca");
                            else {
                                prenotazione = true;
                                isTitComm = false;
                                titMode = false;
                                autMode = false;
                                isAutComm = false;
                                sendMsg(chatId, "Inserisci il numero del libro a cui sei interessato (tra quelli cercati in precedenza)");
                            }
                    }
                }

                else if(prenotazione){
                        int libroPrenotato = Integer.parseInt(message);

                        if(!(libroPrenotato <= 0) && !(libroPrenotato > libriFound.size())){
                            Archives.WriteOnPrenotati(libriFound.get(libroPrenotato).idInv, String.valueOf(chatId));
                            sendMsg(chatId, libriFound.get(libroPrenotato).tit.toUpperCase() + " prenotato!");
                        }
                        else
                            sendMsg(chatId, "Scelta inesistente");

                    prenotazione = false;
                }

                else if(isAutComm && message.equals("Cerca Autore"))
                    sendMsg(chatId, "Stai già cercando un autore");

                else if(isAutComm){
                    isAutComm = false;
                    libriFound.clear();

                    Searcher searcher = new Searcher("autors");
                    libriFound = searcher.search(message);

                    if(!libriFound.isEmpty()) {
                        SendMessage sendMessage = InlineKeyboardBuilder.create(chatId).setText("Risultati Ricerca:").autors(libriFound).build();
                        try {
                            sendApiMethod(sendMessage);
                        } catch (TelegramApiException e) {
                            sendMsg(chatId, "ERRORE!");
                        }
                    }
                    else
                        sendMsg(chatId, " Nessun autore trovato");
                }

                else if(isTitComm && message.equals("Cerca Libro"))
                    sendMsg(chatId, "Stai già cercando un libro");

                else if(isTitComm){
                    isTitComm = false;
                    libriFound.clear();

                    Searcher searcher = new Searcher("titles");
                    libriFound = searcher.search(message);

                    if(!libriFound.isEmpty()) {
                        SendMessage sendMessage = InlineKeyboardBuilder.create(chatId).setText("Risultati Ricerca:").titles(libriFound).build();
                        try {
                            sendApiMethod(sendMessage);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                        sendMsg(chatId, "Nessun libro trovato");
                }
            }).start();


        }
        if(update.hasCallbackQuery()) {
            new Thread(() -> {
                String out = "";
                String callbackData = update.getCallbackQuery().getData();

                if(autMode){
                    for (Libro l :
                            libriFound)
                        if(callbackData.equals(l.aut))
                            out += "\n"+ libriFound.indexOf(l)+ ". " + l.tit.toUpperCase() + " di " + l.aut.toUpperCase() + "\n" +
                                     l.edi + ", " + l.luo.toUpperCase() + "\n" +
                                     l.pub.toUpperCase() + "\n";
                    sendMsg(chatId, out);
                }

                else if(titMode){
                    for (Libro l :
                            libriFound)
                        if(callbackData.equals(l.tit))
                            out += "\n" + libriFound.indexOf(l)+ ". " + l.tit.toUpperCase() + " di " + l.aut.toUpperCase() + "\n" +
                                    l.edi + ", " + l.luo.toUpperCase() + "\n" +
                                    l.pub.toUpperCase() + "\n";
                    sendMsg(chatId, out);

                    if(prenotazione){
                        sendMsg(chatId, "Inserisci il numero del libro a cui sei interessato");
                    }
                }
            }).start();
        }
    }


    public synchronized void sendMsg(long chatId, String s) {
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);

        try {
            if(s.equals(""))
                execute(sendMessage.setText("ERRORE!"));
            else
                execute(sendMessage);
        } catch (TelegramApiException e) {
            sendMsg(chatId, s);
        }
    }

    public synchronized void setButtons(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton("Cerca Autore"));
        keyboardFirstRow.add(new KeyboardButton("Cerca Titolo"));
        keyboard.add(keyboardFirstRow);

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton("Prenota"));
        keyboard.add(keyboardSecondRow);

        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    public synchronized void answerCallbackQuery(String callbackId, String message) {
        AnswerCallbackQuery answer = new AnswerCallbackQuery();
        answer.setCallbackQueryId(callbackId);
        answer.setText(message);
        answer.setShowAlert(false);
        try {
            answerCallbackQuery(answer);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "bot.BibliotecaZuccante";
    }

    @Override
    public String getBotToken() {
        return "1113538359:AAFv6q60iE3khBwyJP5UUvNHUItGRARUf_c";
    }

    @Override
    public void onClosing() {

    }


}
