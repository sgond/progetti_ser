package bot;

public class CSVLoaderException extends Exception{
    public CSVLoaderException() {
        super("The file loaded is not a CSV file");
    }
}
